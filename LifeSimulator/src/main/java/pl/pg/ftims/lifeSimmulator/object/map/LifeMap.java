package pl.pg.ftims.lifeSimmulator.object.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pg.ftims.lifeSimmulator.constants.Config;
import pl.pg.ftims.lifeSimmulator.exception.LifeException;
import pl.pg.ftims.lifeSimmulator.exception.MapTooSmallException;
import pl.pg.ftims.lifeSimmulator.exception.UnreognizeAnimalTypeException;
import pl.pg.ftims.lifeSimmulator.exception.lifeException.DieTimeException;
import pl.pg.ftims.lifeSimmulator.object.animate.Animal;
import pl.pg.ftims.lifeSimmulator.object.animate.Cat;
import pl.pg.ftims.lifeSimmulator.object.animate.Dog;
import pl.pg.ftims.lifeSimmulator.object.inanimate.Inanimate;
import pl.pg.ftims.lifeSimmulator.object.inanimate.Lake;
import pl.pg.ftims.lifeSimmulator.object.inanimate.Tree;

/**
 *
 *
 * @author Janusz Sokołow
 */
public class LifeMap {

    private static final Logger log = LogManager.getLogger(LifeMap.class);

    private Random generator = new Random();

    int width, height;
    Field[][] fields;
    Map<String, Animal> animals = new HashMap<>();
    Map<String, Inanimate> inanimated = new HashMap<>();

    private int treesAmount = 0;
    private int lakesAmount = 0;
    private int inanimateObjects = 0;
    private int animateObjects = 0;

    public LifeMap(int width, int height) {
        this.fields = new Field[width][height];
        this.width = width;
        this.height = height;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                fields[i][j] = new Field();
            }
        }
        log.debug("SUCCESFULLY generate map. Now we got: " + objectsAmount() + " objects.");
    }

    public LifeMap() {
        this(Config.MAP_WIDTH, Config.MAP_HEIGHT);
    }

    public void initWorld() throws LifeException {
        initWorld(Config.INIT_INANIMATE_AMOUNT);
    }

    public void initWorld(int inanimateObjectsAmount) throws LifeException {
        if (inanimateObjectsAmount > freeSpace()) {
            throw new MapTooSmallException();
        }
        for (int i = 0; i < inanimateObjectsAmount; i++) {
            int x = generator.nextInt(width);
            int y = generator.nextInt(height);
            Field field = null;
            while (field == null || !field.isFree()) {
                x = generator.nextInt(width);
                y = generator.nextInt(height);
                field = fields[x][y];
            }

            Inanimate object = generateRandomInanimate();
            field.putInanimate(object);
            inanimated.put(object.getName(), object);
            log.debug("Inanimated " + object.getName() + " has been just initialized on field ( " + x + "|" + y + ")");
            inanimateObjects++;
        }
    }

    public void initLife() throws LifeException {
        initLife(Config.INIT_ANIMAL_AMOUNT);
    }

    public void initLife(int aniamalsAmount) throws LifeException {

        if (aniamalsAmount > freeSpace()) {
            throw new MapTooSmallException();
        }
        for (int i = 0; i < aniamalsAmount; i++) {
            int x = generator.nextInt(width);
            int y = generator.nextInt(height);
            Field field = null;
            while (field == null || !field.isFree()) {
                x = generator.nextInt(width);
                y = generator.nextInt(height);
                field = fields[x][y];
            }

            Animal object = generateRandomAnimal(x, y);
            field.putAnimal(object);
            animals.put(object.getName(), object);
            animateObjects++;
            log.debug("Animal " + object.getName() + " has been just initialized on field ( " + x + "|" + y + ")");
        }
    }

    public void startLiving() {
        int i = 0;
        while (animals.size() > 0) {
            log.debug("START CYCLE NUMBER: " + i++);
            doCycle();
        }
    }

    private void doCycle() {

        List<String> keysToRemove = new LinkedList<>();
        Iterator<String> keysIterator = animals.keySet().iterator();
//        animals.keySet().forEach(animalName -> {
        while (keysIterator.hasNext()) {
            String animalName = keysIterator.next();
            Animal animal = animals.get(animalName);

            try {
                animal.move();

                int[] direction = generateRandomDirection();
                if (direction[0] == 0 & direction[1] == 0) {
                    log.debug("Animal decide to stay at place");
                } else {
                    int newX = animal.getXPosition() + direction[0];
                    int newY = animal.getYPosition() + direction[1];
                    Field newField = fields[newX][newY];

                    Object object = newField.getObject();
                    if (object == null) {
                        newField.putAnimal(animal);
                        fields[animal.getXPosition()][animal.getYPosition()].removeObject();

                    } else if (object instanceof Animal) {
                        log.debug(animal.getName() + " say " + animal.call() + " to : " + ((Animal) object).getName());
                    } else if (object instanceof Inanimate) {
                        if (((Inanimate) object).allowToGetAt(animal)) {
                            newField.putAnimal(animal);
                            fields[animal.getXPosition()][animal.getYPosition()].removeObject();
                        };
                    }
                }
            } catch (LifeException ex) {

            } catch (DieTimeException ex) {
                log.debug("It's time to die for: " + animal);
                int x = animal.getXPosition();
                int y = animal.getYPosition();
                fields[x][y].removeObject();
                keysToRemove.add(animalName);
                animateObjects--;
            } catch (ArrayIndexOutOfBoundsException ex) {
                log.debug("WARN: " + animal + " tries to get out of map, I can't allow that!");
            }
        }
//        });
        keysToRemove.forEach(key -> {
            animals.remove(key);
        });
    }

    private int objectsAmount() {
        return animateObjects;
    }

    private int freeSpace() {
        return (this.height * this.width) - objectsAmount();
    }

    private Animal generateRandomAnimal(int x, int y) throws UnreognizeAnimalTypeException {
        Animal object;
        switch (generator.nextInt(2)) {
            case 0:
                object = new Cat();
                break;
            case 1:
                object = new Dog();
                break;
            default:
                throw new UnreognizeAnimalTypeException();
        }
        object.place(x, y);
        return object;
    }

    private Inanimate generateRandomInanimate() throws UnreognizeAnimalTypeException {
        Inanimate object;
        switch (generator.nextInt(2)) {
            case 0:
                object = new Tree(++treesAmount);
                break;
            case 1:
                object = new Lake(++lakesAmount);
                break;
            default:
                throw new UnreognizeAnimalTypeException();
        }
        return object;
    }

    private int[] generateRandomDirection() {
        int[] result = new int[2];
        result[0] = generateRandomShift();
        result[1] = generateRandomShift();
        return result;
    }

    private int generateRandomShift() {
        switch (generator.nextInt(8)) {
            case 0:
            case 1:
            case 2:
                return -1;
            case 3:
            case 4:
                return 0;
            case 5:
            case 6:
            case 7:
                return 1;
            default:
                return 0;
        }
    }
}
