package pl.pg.ftims.lifeSimmulator.object.inanimate;

import pl.pg.ftims.lifeSimmulator.object.animate.Animal;

/**
 *
 * @author Janusz Sokołow
 */
public interface Inanimate{
    
    boolean allowToGetAt (Animal animal);
    String getName ();
    
}
