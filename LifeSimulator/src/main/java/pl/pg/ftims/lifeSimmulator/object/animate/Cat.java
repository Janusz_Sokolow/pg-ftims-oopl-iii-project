package pl.pg.ftims.lifeSimmulator.object.animate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pl.pg.ftims.lifeSimmulator.constants.Config;

/**
 *
 * @author Janusz Sokołow
 */
public class Cat extends Animal {

    private static final List<String> typicalNames = new ArrayList<String>(
            Arrays.asList("Mruczek", "Murka", "Kot", "Ryży", "Filimon", "Catrine", "Charlie", "Feliks"));

    public Cat() {
        super(typicalNames.get(generator.nextInt(typicalNames.size())), generator.nextInt(Config.CAT_LIFE_TIME));
        this.lifeTime = Config.CAT_LIFE_TIME;
    }

    @Override
    public String call() {
        return "Miał miał";
    }

    
    @Override
    public String toString() {
        return "I'm a CAT " + name + "(" + xPosition + "|" + yPosition + ")";
    }
}
