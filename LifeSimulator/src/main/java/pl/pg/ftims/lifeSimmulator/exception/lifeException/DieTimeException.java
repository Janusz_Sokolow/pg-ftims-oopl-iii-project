package pl.pg.ftims.lifeSimmulator.exception.lifeException;

/**
 *
 * @author Janusz Sokołow
 */
public class DieTimeException extends Exception{

    public DieTimeException() {
        super("It's time to die.");
    }
}
