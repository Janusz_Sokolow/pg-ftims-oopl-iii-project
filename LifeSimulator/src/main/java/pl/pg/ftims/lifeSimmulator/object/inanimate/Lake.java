package pl.pg.ftims.lifeSimmulator.object.inanimate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pg.ftims.lifeSimmulator.object.animate.Animal;

/**
 *
 * @author Janusz Sokołow
 */
public class Lake implements Inanimate {

    private static final Logger log = LogManager.getLogger(Lake.class);
    private final String name;
    
    public Lake(int nr) {
        this.name = "Lake_" + nr;
    }
    
    @Override
    public boolean allowToGetAt(Animal animal) {
        switch (animal.getClass().getSimpleName()) {
            case "Dog":
                log.debug("INFO: Here is a dog tring get wash!");
                return true;
            case "Cat":
                log.debug("WARN: Cat is afraid of water! I can't allow it get here ");
                return false;
            default:
                log.debug("WARN: I don't know this animal type, I'm confuse to allow him get into me!");
                return false;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }
}
