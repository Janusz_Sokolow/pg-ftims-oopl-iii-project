package pl.pg.ftims.lifeSimmulator.exception;

/**
 *
 * @author Janusz Sokołow
 */
public class UnrecognizeInanimateTypeException extends LifeException{

    public UnrecognizeInanimateTypeException() {
        super("Trying generate not existing inanimate type.");
    }
    
    
}
