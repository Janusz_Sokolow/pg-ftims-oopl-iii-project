package pl.pg.ftims.lifeSimmulator.object.map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pg.ftims.lifeSimmulator.exception.FieldNotEmptyException;
import pl.pg.ftims.lifeSimmulator.exception.LifeException;
import pl.pg.ftims.lifeSimmulator.object.animate.Animal;
import pl.pg.ftims.lifeSimmulator.object.inanimate.Inanimate;

/**
 * Implementation of one field on
 * @see Map
 *
 * @author Janusz Sokołow
 */
public class Field {

    private static final Logger log = LogManager.getLogger(Field.class);

    private String name;
    private Object object;

    public Field() {
    }

    public boolean isFree() {
        return name == null;
    }

    public void putAnimal(Animal object) throws LifeException {
        if (!isFree()) {
            throw new FieldNotEmptyException();
        }
        this.object = object;
        this.name = object.getName();
    }
    
     public void putInanimate(Inanimate object) throws LifeException {
        if (!isFree()) {
            throw new FieldNotEmptyException();
        }
        this.object = object;
        this.name = object.getName();
    }
    
    public void removeObject() {
        this.name = null;
    }

    public String getName() {
        return isFree() ? null : name;
    }

    public Object getObject() {
        return isFree() ? null : object;
    }

    @Override
    public String toString() {
        String message = "This field is ";
        if (isFree()) {
            message += "empty";
        } else {
            message += "placed by : " + name;
        }
        return message;
    }
}
