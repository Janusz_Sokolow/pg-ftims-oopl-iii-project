package pl.pg.ftims.lifeSimmulator.object.inanimate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pg.ftims.lifeSimmulator.object.animate.Animal;

/**
 *
 * @author Janusz Sokołow
 */
public class Tree implements Inanimate {

    private static final Logger log = LogManager.getLogger(Tree.class);
    private final String name;

    public Tree(int nr) {
        this.name = "Tree_" + nr;
    }

    @Override
    public boolean allowToGetAt(Animal animal) {
        switch (animal.getClass().getSimpleName()) {
            case "Dog":
                log.debug("WARN: Here is dog tring get at tree!");
                return false;
            case "Cat":
                log.debug("INFO: Cat can get at tree!");
                return true;
            default:
                log.debug("WARN: I don't know this animal type, I'm confuse to allow him get at me!");
                return false;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }
}
