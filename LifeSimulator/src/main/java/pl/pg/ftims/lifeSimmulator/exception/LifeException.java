package pl.pg.ftims.lifeSimmulator.exception;

/**
 *
 * @author Janusz Sokołow
 */
public class LifeException extends Exception {

    public LifeException() {
        super();
    }

    public LifeException(String message) {
        super(message);
    }
}
