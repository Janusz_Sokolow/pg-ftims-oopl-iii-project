package pl.pg.ftims.lifeSimmulator.exception;

/**
 *
 * @author Janusz Sokołow
 */
public class FieldNotEmptyException extends LifeException{

    public FieldNotEmptyException() {
        super("Trying put object into not empty field.");
    }
    
    
}
