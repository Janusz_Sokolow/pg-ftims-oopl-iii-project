
package pl.pg.ftims.lifeSimmulator.constants;

/**
 * Class contains constants required to configure aplication parameters
 * 
 * Be careful to change parameters - it might make application working wrong.  
 * 
 * 
 * @author Janusz Sokołow
 */
public class Config {
    public static final int INIT_ANIMAL_AMOUNT = 20;    /* SUGEST 30 */
    public static final int INIT_INANIMATE_AMOUNT = 40; /* SUGEST 20 */
    
    public static final int MAP_WIDTH = 10;             /* SUGEST 10 */
    public static final int MAP_HEIGHT = 10;            /* SUGEST 10 */
    
    public static final int DOG_LIFE_TIME = 10;         /* SUGEST 10 */
    
    public static final int CAT_LIFE_TIME = 6;          /* SUGEST 6 */
    
}
