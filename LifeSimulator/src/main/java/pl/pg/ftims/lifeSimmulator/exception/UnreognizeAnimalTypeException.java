package pl.pg.ftims.lifeSimmulator.exception;

/**
 *
 * @author Janusz Sokołow
 */
public class UnreognizeAnimalTypeException extends LifeException{

    public UnreognizeAnimalTypeException() {
        super("Trying generate not existing animal type.");
    }
    
    
}
