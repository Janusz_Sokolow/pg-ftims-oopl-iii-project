package pl.pg.ftims.lifeSimmulator.object.animate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pg.ftims.lifeSimmulator.exception.lifeException.DieTimeException;

/**
 *
 * @author Janusz Sokołow
 */
public abstract class Animal {

    protected static final Logger log = LogManager.getLogger(Animal.class);

    protected static final Random generator = new Random();
    private static List<String> existingAnimals = new ArrayList<>();

    protected String name;
    protected int xPosition, yPosition;
    protected int years, lifeTime;

    public Animal(String name, int years) {
        if (existingAnimals.contains(name)) {
            name += "_" + generator.nextInt(10);
        }
        while (existingAnimals.contains(name)) {
            name += generator.nextInt(10);
        }
        existingAnimals.add(name);
        this.name = name;
        this.years = years;
    }

    public abstract String call();

    public String getName() {
        return name;
    }

    public int getXPosition() {
        return xPosition;
    }

    public int getYPosition() {
        return yPosition;
    }

    public void place(int x, int y) {
        this.xPosition = x;
        this.yPosition = y;
    }

    public void move() throws DieTimeException {
        this.years++;
        if (lifeTime < this.years) {
            throw new DieTimeException();
        }
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash += hash * 31 + name.hashCode();
        hash += hash * 17 + lifeTime;
        hash += this.getClass().getSimpleName().hashCode();
        return hash;
    }

}
