package pl.pg.ftims.lifeSimmulator.object.animate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pl.pg.ftims.lifeSimmulator.constants.Config;
import static pl.pg.ftims.lifeSimmulator.object.animate.Animal.generator;

/**
 *
 * @author Janusz Sokołow
 */
public class Dog extends Animal {

    private static final List<String> typicalNames = new ArrayList<String>(
            Arrays.asList("Łajka", "Bobik", "Reksio", "Szarik", "Szczek", "Maks", "Jack"));

    public Dog() {
        super(typicalNames.get(generator.nextInt(typicalNames.size())), generator.nextInt(Config.DOG_LIFE_TIME));
        this.lifeTime = Config.DOG_LIFE_TIME;
    }

    @Override
    public String call() {
        return "Hau hau";
    }

    @Override
    public String toString() {
        return "I'm a DOG " + name + "(" + xPosition + "|" + yPosition + ")";
    }

}
