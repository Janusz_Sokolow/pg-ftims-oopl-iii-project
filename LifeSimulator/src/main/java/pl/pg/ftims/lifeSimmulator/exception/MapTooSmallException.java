package pl.pg.ftims.lifeSimmulator.exception;

/**
 *
 * @author Janusz Sokołow
 */
public class MapTooSmallException extends LifeException{

    public MapTooSmallException() {
        super("Required amount of objects can't be placed on the map.");
    }
    
    
}
