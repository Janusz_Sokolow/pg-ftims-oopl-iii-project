/**
 * pl.pg.ftims.jsokolow.main
 * 
 * Verson 1.0.0-SNAPSHOT
 *
 * Copyright (c) 2016, Janusz Sokołow. All rights reserved.
 */
package pl.pg.ftims.lifeSimmulator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pg.ftims.lifeSimmulator.exception.LifeException;
import pl.pg.ftims.lifeSimmulator.object.map.LifeMap;

/**
 *
 * @author Janusz Sokołow
 */
public class Runner {
    
    private static final Logger log = LogManager.getLogger(Runner.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        LifeMap lifeMap = new LifeMap();
        
        try {
            lifeMap.initWorld();
            lifeMap.initLife();
            
            lifeMap.startLiving();
          
            // TODO: Ask user about cycles amount
            
            // TODO:  To sum up aplication life (start animals amount, finish animal amoit, number of meetings )
        } catch (LifeException ex) {
            log.error(ex);
            ex.printStackTrace();
        }
    }

}
