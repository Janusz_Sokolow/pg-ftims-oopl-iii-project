package pl.pg.ftims.lifeSimmulator.object.animate;

/**
 *
 * @author Janusz Sokołow
 */
public abstract class Animal {
    
    protected static Long Meetings = new Long(0);
    
    public abstract void go();
    public abstract void run();
    public abstract void call();
}
