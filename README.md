#Life Simulator#
It's object-oriented programming project done by Janusz Sokołow


## Author
	Janusz Sokołow

## Description
	Application is animal life simulator including initialization world (animated and inanimate), 
	animals moving, speaking each others

## How to compile and run
	This is Java project using Maven project management tools, so to run it you have to just clone repository, 
	open, build  and run project. All information about project running will be shown in console, 
	hope so you won't feel confused about it.

## Additional informations
	There is some features I want to implement like asking application user about cycles amount, 
	resuming application working, couple animals. Project maybe will be continued after examination session.

## Requirements for project

+ Project needs to have at least 2-level class inheritance present (regardless if base class is abstract or not).
+ Example of using class field polymorphic binding needs to be present in the source code.
+ Usage of at least two custom interfaces is mandatory.
+ Project code needs to use two types of collections: List or Set and Map. Collections implementation is for student to decide.
+ Example of collection iteration needs to be present in the source code.
+ Custom exception needs to be created used in the project.